#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"
#define BLYNK_USE_DIRECT_CONNECT

// You could use a spare Hardware Serial on boards that have it (like Mega)
#include <SoftwareSerial.h>
SoftwareSerial BluetoothSerial(5, 6); // 5=tx, 6=rx

#define BLYNK_PRINT BluetoothSerial
#include <BlynkSimpleSerialBLE.h>

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "ZBCub1IrPsDEsqW4ocvphnbuny-bnh5_";

// class default I2C address è 0x68 per il MPU6050 collegato ai pin A4 A5
MPU6050 mpu;


#define OUTPUT_READABLE_YAWPITCHROLL //commenstare se non si vuole visualizzare gli angoli di inclinazione yaw/pitch/roll (in gradi)

//#define OUTPUT_TEAPOT  //per visualizzare in 3D il movimento del sensore 6050 sullo schermo del pc toglere il commento. (commentare OUTPUT_READABLE_YAWPITCHROLL se no non va!!!)



#define LED_PIN 13 
bool blinkState = false;

// MPU control/status vars

bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternione container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

// packet structure for InvenSense teapot demo
uint8_t teapotPacket[14] = { '$', 0x02, 0,0, 0,0, 0,0, 0,0, 0x00, 0x00, '\r', '\n' };



// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}

// MOJ KOD**********************-------************--------***************-*******************************************
#include <PID_v1.h>
#include <Wire.h>
#include <SoftwareSerial.h>

float Pitch = 0;
float Roll = 0;
float Yaw = 0;

int throttle = 0;

//------------- Assegno i motori ai pin 3,9,10,11 del arduino nano --------------------

int motor1 = 10; 
int motor2 = 11;
int motor3 = 3;
int motor4 = 9;

float speed1 = 0;
float speed2 = 0;
float speed3 = 0;
float speed4 = 0;

double SetpointP, InputP, OutputP;
double SetpointR, InputR, OutputR;
double SetpointY, InputY, OutputY;

double KpP=0.29, KiP=0.081, KdP=0.0122;  //0.26      0.027   0 bilo
double KpR= 0.32, KiR=0.07, KdR=0.014;// 
double KpY=0.28, KiY=0.01, KdY=0.009;

PID PidPitch(&InputP, &OutputP, &SetpointP, KpP, KiP, KdP, REVERSE);
PID PidRoll(&InputR, &OutputR, &SetpointR, KpR, KiR, KdR, REVERSE);
PID PidYaw(&InputY, &OutputY, &SetpointY, KpY, KiY, KdY, REVERSE);



// ================================================================
// ===                      INITIAL SETUP                       ===
// ================================================================

void setup() {
    // join I2C bus (I2Cdev library doesn't do this automatically)
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
        TWBR = 24; // 400kHz I2C clock (200kHz if CPU is 8MHz)
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif

    BluetoothSerial.begin(9600);
    BluetoothSerial.println("Waiting for connections...");

    Blynk.begin(BluetoothSerial, auth);
    // initialize serial communication
    // (115200 chosen because it is required for Teapot Demo output, but it's
    // really up to you depending on your project)
    Serial.begin(115200);
    while (!Serial);

    // initialize device
    Serial.println(F("Initializing I2C devices..."));
    mpu.initialize();

    // verify connection
    Serial.println(F("Testing device connections..."));
    Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

    // wait for ready
    Serial.println(F("\nSend any character to begin DMP programming and demo: "));
    //while (Serial.available() && Serial.read()); // empty buffer
    //while (!Serial.available());                 // wait for data
    while (Serial.available() && Serial.read()); // empty buffer again

    // load and configure the DMP
    Serial.println(F("Initializing DMP..."));
    devStatus = mpu.dmpInitialize();

    // gyro offsets
    mpu.setXGyroOffset(65);
    mpu.setYGyroOffset(-20);
    mpu.setZGyroOffset(35);
    mpu.setXAccelOffset(-699);
    mpu.setYAccelOffset(1097);
    mpu.setZAccelOffset(1424); 

    // make sure it worked (returns 0 if so)
    if (devStatus == 0) {
        // turn on the DMP, now that it's ready
        Serial.println(F("Enabling DMP..."));
        mpu.setDMPEnabled(true);

        // enable Arduino interrupt detection
        Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));
        attachInterrupt(0, dmpDataReady, RISING);
        mpuIntStatus = mpu.getIntStatus();

        // set our DMP Ready flag so the main loop() function knows it's okay to use it
        Serial.println(F("DMP ready! Waiting for first interrupt..."));
        dmpReady = true;

        // get expected DMP packet size for later comparison
        packetSize = mpu.dmpGetFIFOPacketSize();
    } else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        Serial.print(F("DMP Initialization failed (code "));
        Serial.print(devStatus);
        Serial.println(F(")"));
    }

    // configure LED for output
    pinMode(LED_PIN, OUTPUT);


    //MOJ KOD U SETUP*******************************************************************************************
      Serial.setTimeout(5);
      /*
      pinMode(motor1, OUTPUT);
      pinMode(motor2, OUTPUT);
      pinMode(motor3, OUTPUT);
      pinMode(motor4, OUTPUT);
    */
    
    PidPitch.SetMode(AUTOMATIC);
    PidRoll.SetMode(AUTOMATIC);
    PidYaw.SetMode(AUTOMATIC);

    PidPitch.SetSampleTime(1);
    PidRoll.SetSampleTime(1);
    PidYaw.SetSampleTime(1);

    PidPitch.SetOutputLimits(0, 40);
    PidRoll.SetOutputLimits(0, 40);
    PidYaw.SetOutputLimits(0, 40);

    SetpointP = 0;
    SetpointR = 0;
    SetpointY = Yaw;
}


    int input_degree;
// ================================================================
// ===                    MAIN PROGRAM LOOP                     ===
// ================================================================

void loop() {
    // se il programma crasha 
    if (!dmpReady) return;

    Blynk.run();

    // reset interrupt flag and get INT_STATUS byte
    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();

    // get current FIFO count
    fifoCount = mpu.getFIFOCount();

    // check for overflow (this should never happen unless our code is too inefficient)
    if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        Serial.println(F("FIFO overflow!"));

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
    } else if (mpuIntStatus & 0x02) {
        // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;



        #ifdef OUTPUT_READABLE_YAWPITCHROLL
            // display Euler angles in degrees
            mpu.dmpGetQuaternion(&q, fifoBuffer);
            mpu.dmpGetGravity(&gravity, &q);
            mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
            Serial.print("ypr\t");
            Serial.print(ypr[0] * 180/M_PI);
            Serial.print("\t");
            Serial.print(ypr[1] * 180/M_PI);
            Serial.print("\t");
            Serial.println(ypr[2] * 180/M_PI);
        #endif

          
      #ifdef OUTPUT_TEAPOT
            // display quaternion values in InvenSense Teapot demo format:
            teapotPacket[2] = fifoBuffer[0];
            teapotPacket[3] = fifoBuffer[1];
            teapotPacket[4] = fifoBuffer[4];
            teapotPacket[5] = fifoBuffer[5];
            teapotPacket[6] = fifoBuffer[8];
            teapotPacket[7] = fifoBuffer[9];
            teapotPacket[8] = fifoBuffer[12];
            teapotPacket[9] = fifoBuffer[13];
            Serial.write(teapotPacket, 14);
            teapotPacket[11]++; // packetCount, loops at 0xFF on purpose
        #endif

        // blink LED to indicate activity
        blinkState = !blinkState;
        digitalWrite(LED_PIN, blinkState);
    }

    //KOD ZA LOOP*********************************************************************************************************
    Yaw =   ypr[0] * 180/M_PI;
    Pitch = ypr[1] * 180/M_PI;
    Roll =  ypr[2] * 180/M_PI;
    
    /*
    if(Serial.available()>0)
    {
        input_degree = Serial.parseInt();
        //Serial.println(input_degree);
    }*/

    if(input_degree > 0 && input_degree < 1024 )
{

  throttle = map(input_degree, 0, 1023, 0, 170);
  

  if(Yaw <0)
  {
    InputY = - Yaw;
    PidYaw.Compute();
    }
    else
    {
      InputY = Yaw;
      PidYaw.Compute();
      }
    
  if(Pitch < 0)
{
 InputP = - Pitch;
 PidPitch.Compute();
        speed4 = OutputP+ throttle;
        speed1 = -OutputP+ throttle;
  }
  else
  {
        InputP = Pitch;
        PidPitch.Compute();
        speed4 = -OutputP+ throttle;
        speed1 = OutputP+ throttle;
    }
    if(Roll < 0)
{
        InputR= - Roll;
        PidRoll.Compute();
        speed2 = OutputR+ throttle;
        speed3 = -OutputR+ throttle;
  }
  else
  {
        InputR= Roll;
        PidRoll.Compute();
        speed2 = -OutputR+ throttle;
        speed3 = OutputR+ throttle;
    }

    if(Yaw < 0)
    {
        speed4 += OutputY;
        speed1 += OutputY;
        speed2 -= OutputY;
        speed3 -= OutputY;
      }
      else
      {
        speed2 += OutputY;
        speed3 += OutputY;
        speed4 -= OutputY;
        speed1 -= OutputY;
        }
        
    
    if(speed1<0)
    {
      speed1= 0;
      }
      if(speed2<0)
    {
      speed2= 0;
      }
     if(speed3<0)
    {
      speed3= 0;
      }
      if(speed4<0)
    {
      speed4= 0;
      }
      motori(speed1, speed2, speed3, speed4);
  }
    else
    {
      motori(0,0,0,0);
     }

}

void motori(float s1, float s2, float s3, float s4)
{
        analogWrite(motor1,s1);
        analogWrite(motor4,s4);
        analogWrite(motor2,s2);
        analogWrite(motor3,s3);
}

  BLYNK_WRITE(V3)//Roll
  {
   InputR = param.asInt();
  }

  BLYNK_WRITE(V1)//YAW
  {
   //SliderValue = param[0].asInt(); // Assigning incoming value from pin V3 to a variable
   //y=param[1].asInt();
   InputY = param.asInt();
  }
  BLYNK_WRITE(V2)//Pitch
  {
   
   InputP = param.asInt();
  }

  BLYNK_WRITE(V0)//PITCH
  {
  
   input_degree = param.asInt();
  }
  
  