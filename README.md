# Bluetooth Quadcopter #

Autore: Vladislav Chircu

Descrizione: Il progetto consiste nella realizzazione di un quadrilatero che sarà in grado di restare in equilibrio autonomamente usando un giroscopio e accelerometro. Il controllo del quadrilatero sarà effettuato tramite un applicazione che si collegherà con il drone tramite il blutooth. 

### Hardware e materiale utilizzato: ###

* Arduino nano
* Modulo GY-521 MPU-6050
* DX2205 2300KV Brushless Motor 2CW 2CCW da 2-4S 
* Telaio a forma di x con Interasse: 220mm (Per supporto propelle fino a 5 pollici)
* 4 in 1 50A BLHeli ESC BLHeli 3-6s Supporto Lipo 
* Modulo Bluetooth hc 06
* Millefori 
* 4 Eliche da 5'
* Vari piedini maschio e femmina
* Batteria LiPo 14.8V 4S 1300mAh
* Condensatore da 470uF

### Librerie utilizzate: ###

* libreria Servo per arduinio nano
* MPU6050 I2C device class, 6-axis MotionApps 2.0 implementation
* PID_v1.h
* SoftwareSerial.h
* BLYNK_USE_DIRECT_CONNECT

### Limitazioni del Progetto ###

* Usando arduino nano non si ha la possibilità di aggiungere molti sensori aggiuntivi come il barometro e magnetometro per usarli contemporaneamente quindi per sfruttarlo al meglio andrebbe cambiata con una board con piu pin a disposizione.

